#include <iostream>

using namespace std;

struct Node{
    int data;
    Node *next;
};
Node *head=NULL;
Node *last=NULL;

void insert(int a)
{
    Node *node = new Node;
    node->data=a;
    node->next=NULL;
    if(head==NULL)
    {
        head=node;
        last=head;
    }
    else if(head->next==NULL)
    {
        head->next=node;
        last=node;
    }
    else
    {
        last->next=node;
        last=node;
    }
}

void print()
{
    if(head==NULL)
    {
        cout<<"linked list is empty.";
    }
    else
    {
        cout<<"Linked list formed is: "<<"\n";
        Node *temp = head;
        while((temp)!=NULL)
        {
            cout<<temp->data<<"\n";
            temp=temp->next;
        }
    }
}

int main()
{
    int n;
    cout<<"Enter the number of elements: ";
    cin>>n;
    for(int i=0;i<n;i++)
    {
        int a;
        cin>>a;
        insert(a);
    }
    print();
}