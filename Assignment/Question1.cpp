#include<iostream>

using namespace std;
void reverseArray(int *arr, int n)
{
    int temp=0;
    int mid=(n-1)/2;
    int j=(n-1);
    //cout<<mid;
    for(int i=0;i<=mid;i++)
    {
        temp=arr[i];
        arr[i]=arr[j];
        arr[j]=temp;
        j--;
    }
    cout<<"\n"<<"Array after reversal: ";
    for(int i=0;i<n;i++)
    {
        cout<<arr[i]<<"\t";
    }
    cout<<"\n";
}
int main()
{
    int arr[100];
    int n;
    cout<<"Enter the number of elements: ";
    cin>>n;
    for(int i=0;i<n;i++)
    {
        cin>>arr[i];
    }
    cout<<"\n"<<"Array before reversal: ";
    for(int i=0;i<n;i++)
    {
        cout<<arr[i]<<"\t";
    }
    reverseArray(arr,n);
}